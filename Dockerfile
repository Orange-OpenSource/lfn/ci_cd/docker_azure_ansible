FROM python:3
MAINTAINER Sylvain Desbureaux <sylvain@desbureaux.fr>

ARG VCS_REF
ARG BUILD_DATE
ARG ANSIBLE_VERSION=2.9.0

LABEL org.label-schema.vcs-url="https://github.com/ansible/ansible" \
      org.label-schema.url="https://www.ansible.com" \
      org.label-schema.name="ansible" \
      org.label-schema.docker.dockerfile="/Dockerfile" \
      org.label-schema.license="MIT" \
      org.label-schema.build-date=$BUILD_DATE

WORKDIR /playbooks

RUN apt update && \
    apt install -y build-essential \
                   git \
                   jq \
                   libffi-dev \
                   libssl-dev \
                   netcat-openbsd \
                   openssh-client \
                   rsync \
                   unzip &&\
    rm -rf /usr/bin/python && \
    ln -s /usr/local/bin/python /usr/bin/python && \
    rm -rf /usr/bin/pip && \
    ln -s /usr/local/bin/pip /usr/bin/pip && \
    pip install --upgrade pip && \
    pip install -r https://raw.githubusercontent.com/ansible-collections/azure/dev/requirements-azure.txt && \
    pip install ansible==$ANSIBLE_VERSION jmespath netaddr yq && \
    apt-get remove -y --auto-remove --purge build-essential && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* && \
    rm -rf ~/.cache/pip

RUN mkdir -p /etc/ansible
RUN echo 'localhost ansible_connection=local' > /etc/ansible/hosts

CMD ["ansible", "--version"]
