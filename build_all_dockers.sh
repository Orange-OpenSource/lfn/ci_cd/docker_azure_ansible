#!/usr/bin/env sh
set -e
. ./ansible_versions.sh

# loop on latest ansible versions
for ANSIBLE_VERSION in ${LATEST_MAJOR_VERSIONS}; do
  echo "#######################################################################"
  echo "Build ansible version ${ANSIBLE_VERSION}"
  echo "#######################################################################"

  # set image name based on branch
  if [ "${CI_COMMIT_REF_SLUG}" == "master" ]; then
    REF=""
  else
    REF="-${CI_COMMIT_REF_SLUG}"
  fi

  if [ "${ANSIBLE_VERSION}" == "$(echo -e "${ANSIBLE_VERSION}\n2.9" | sort -Vr | head -n1)" ]; then
    echo "new (collection) mode"
    DOCKERFILE=Dockerfile
  else
    echo "old (legacy) mode"
    DOCKERFILE=Dockerfile.legacy
  fi

  docker build \
    --build-arg BUILD_DATE=$(date -u +”%Y-%m-%dT%H:%M:%SZ”) \
    --build-arg ANSIBLE_VERSION=${ANSIBLE_VERSION} \
    -f "${DOCKERFILE}" \
    -t "${CI_REGISTRY_IMAGE}:${ANSIBLE_VERSION}${REF}" .

  docker push "${CI_REGISTRY_IMAGE}:${ANSIBLE_VERSION}${REF}"

  # Tag latest major version to major version and push (2.7 for 2.7.6)
  docker tag "${CI_REGISTRY_IMAGE}:${ANSIBLE_VERSION}${REF}" "${CI_REGISTRY_IMAGE}:${ANSIBLE_VERSION%.*}${REF}"
  docker push "${CI_REGISTRY_IMAGE}:${ANSIBLE_VERSION%.*}${REF}"


  if [ "${ANSIBLE_VERSION}" == "${LATEST_VERSION}" ]; then
    docker tag "${CI_REGISTRY_IMAGE}:${ANSIBLE_VERSION}${REF}" "${CI_REGISTRY_IMAGE}:latest${REF}";
    docker push "${CI_REGISTRY_IMAGE}:latest${REF}";
  fi
done
